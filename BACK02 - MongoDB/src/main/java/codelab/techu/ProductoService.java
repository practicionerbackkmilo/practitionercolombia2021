package codelab.techu;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductoService {

    @Autowired
    ProductoRepository productoRepository;

    public List<ProductoMongo> findAll() {
        return productoRepository.findAll();
    }

    public Optional<ProductoMongo> findById(String  id) {
        return productoRepository.findById(id);
    }

    public ProductoMongo save(ProductoMongo entity) {
        return productoRepository.save(entity);
    }

    public boolean delete(ProductoMongo entity) {
        try {
            productoRepository.delete(entity);
            return true;
        } catch(Exception ex) {
            return false;
        }
    }
}

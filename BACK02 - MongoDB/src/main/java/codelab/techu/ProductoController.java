package codelab.techu;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/apitechu/")
public class ProductoController {
    @Autowired
    ProductoService productoService;

    /* Get lista de productos */
    @GetMapping("/productos")
    public ResponseEntity<List<ProductoMongo>> getProductos() {
        return new ResponseEntity<>(productoService.findAll(), HttpStatus.OK);
    }

    /* Get producto por id */
    @GetMapping("/productos/{id}")
    public ResponseEntity<ProductoMongo> getProductoId(@PathVariable String id)
    {
        return productoService.findById(id)
                .map(productoMongo -> new ResponseEntity<>(productoMongo, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /* Add nuevo producto */
    @PostMapping(value = "/productos")
    public ResponseEntity<String> postProductos(@RequestBody ProductoMongo newProducto) {
        productoService.save(newProducto);
        return new ResponseEntity<>("Producto creado correctamente", HttpStatus.CREATED);
    }

    /* PUT actualizar producto */
    @PutMapping("/productos")
    public ResponseEntity<ProductoMongo> putProductos(@RequestBody ProductoMongo productoToUpdate){
        ProductoMongo productoPut = productoService.save(productoToUpdate);
        return new ResponseEntity<>(productoPut, HttpStatus.OK);
    }

    @DeleteMapping("/productos")
    public ResponseEntity<String> deleteProductos(@RequestBody ProductoMongo productoToDelete){
        if(productoService.delete(productoToDelete))
            return new ResponseEntity<>("Producto eliminado",HttpStatus.OK);
        else
            return new ResponseEntity<>("Id de Producto no informado",HttpStatus.BAD_REQUEST);

    }
}

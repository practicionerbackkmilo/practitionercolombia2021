package net.techu;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class ProductosController {
    private ArrayList<Producto> listaProductos = null;

    public ProductosController() {
        listaProductos = new ArrayList<>();
        listaProductos.add(new Producto(1, "PR1",27.35));
        listaProductos.add(new Producto(2, "PR2", 18.33));
    }

    /* Get lista de productos */
    @GetMapping(value = "/v2/productos", produces = "application/json")
    public ResponseEntity<ArrayList<Producto>> obtenerListado()
    {
        return new ResponseEntity<>(listaProductos, HttpStatus.OK);
    }

    @GetMapping("/v2/productos/{id}")
    public ResponseEntity<Producto> obtenerProductoPorId(@PathVariable int id)
    {
        Producto productoPorId=null;

        for (Producto p: listaProductos) {
            if(p.getId() == id)
                productoPorId=p;
        }
        if(productoPorId!=null)
            return new ResponseEntity<>(productoPorId, HttpStatus.OK);

        return new ResponseEntity(productoPorId, HttpStatus.NOT_FOUND);

    }

    @GetMapping("/v2/productos/{id}/precio")
    public ResponseEntity<String> obtenerPrecioDeProductoPorId(@PathVariable int id)
    {
        Producto productoPorId=null;

        for (Producto p: listaProductos) {
            if(p.getId() == id) {
                productoPorId = p;
                return new ResponseEntity<>(String.valueOf(productoPorId.getPrecio()), HttpStatus.OK);
            }
        }
        return new ResponseEntity("Producto no encontrado", HttpStatus.NOT_FOUND);

    }

    /* Add nuevo producto */
    @PostMapping(value = "/v2/productos", produces="application/json")
    public ResponseEntity<String> addProducto(@RequestBody Producto productoNuevo) {
        listaProductos.add(productoNuevo);
        return new ResponseEntity<>("Producto creado correctamente", HttpStatus.CREATED);
    }

    /* Add nuevo producto desde URL */
    @PostMapping(value = "/v2/productos/{nom}", produces="application/json")
    public ResponseEntity<String> addProductoConNombre(@PathVariable("nom") String nombre) {
        listaProductos.add(new Producto(99, nombre, 100.5));
        return new ResponseEntity<>("Producto creado correctamente", HttpStatus.CREATED);
    }

    /* PUT actualziacion de producto */
    @PutMapping("/v2/productos/{id}")
    public ResponseEntity<String> updateProducto(@PathVariable int id, @RequestBody Producto productoModificado)
    {

            Producto productoAActualizar=null;
            int indice=0;

            for (Producto p: listaProductos) {
                if(p.getId() == id){
                    productoAActualizar=productoModificado;
                    indice=listaProductos.indexOf(p);
                }
            }
            if(productoAActualizar!=null){
                listaProductos.set(indice, productoAActualizar);
                return new ResponseEntity<>("Producto actualizado correctamente", HttpStatus.OK);
            }
            return new ResponseEntity<>("No se ha encontrado el producto", HttpStatus.NOT_FOUND);

    }

    /* PUT Subir precio a todos los productos */
    @PutMapping("/v2/productos")
    public ResponseEntity<String> subirPrecio()
    {
        ResponseEntity<String> resultado = null;
        for (Producto p:listaProductos) {
            p.setPrecio(p.getPrecio()*1.25);
        }
        resultado = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        return resultado;
    }

    /* DELETE producto por id */
    @DeleteMapping("/v2/productos/{id}")
    public ResponseEntity<String> deleteProducto(@PathVariable int id)
    {
        ArrayList<Producto> productosAEliminar = new ArrayList<>();

        for (Producto p: listaProductos) {
            if(p.getId() == id){
                productosAEliminar.add(p);
            }

        }
        if(!(productosAEliminar.isEmpty())){
            listaProductos.removeAll(productosAEliminar);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>("No se ha encontrado el producto", HttpStatus.NOT_FOUND);


    }

    /* DELETE todos los productos */
    @DeleteMapping("/v2/productos")
    public ResponseEntity<String> deleteTodos()
    {
        ResponseEntity<String> resultado;
        listaProductos.clear();
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    /* PATCH actualizacion de nombre a producto por id  */
    @PatchMapping("/v2/productos/{id}")
    public ResponseEntity<String> actualizarNombreProducto(@PathVariable int id,@RequestBody Producto productoActualizado)
    {
        ArrayList<Producto> productosAActualizar = new ArrayList<>();
        ArrayList<Integer> indice = new ArrayList<>();

        for (Producto p: listaProductos) {
            if(p.getId() == id){
                productosAActualizar.add(productoActualizado);
                indice.add(listaProductos.indexOf(p));
            }
        }
        if(!productosAActualizar.isEmpty()){
            for (int i=0; i<productosAActualizar.size();i++){
                listaProductos.get(indice.get(i)).setNombre(productosAActualizar.get(i).getNombre());
            }

            return new ResponseEntity<>("Producto(s) actualizado(s) correctamente", HttpStatus.OK);
        }
        return new ResponseEntity<>("No se ha encontrado el producto", HttpStatus.NOT_FOUND);
    }
}